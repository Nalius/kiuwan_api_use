<?php
    ob_start();
    include_once("classes/kiuwan_generals.php");
    include_once("classes/kiuwan_apps.php");
    include_once("classes/kiuwan_analisis.php");
    include_once("classes/kiuwan_defects.php");
    //include_once("lib/phpToPDF.php");
    require_once 'lib/dompdf/lib/html5lib/Parser.php';
    //require_once 'lib/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
    //require_once 'lib/dompdf/lib/php-svg-lib/src/autoload.php';
    require_once 'lib/dompdf/src/Autoloader.php';
    Dompdf\Autoloader::register();
    use Dompdf\Dompdf;


    ////////////////////////////

    $application_name = $_GET["application"];

    $construct_array= array();
    $construct_array["name"] = $application_name;

    $kiuwan_generals = new kiuwan_generals();
    $kiuwan_apps = new kiuwan_apps();
    $kiuwan_analysis = new kiuwan_analysis($construct_array);

    $user_info = $kiuwan_generals->get_user_info();
    $last_analysis =  $kiuwan_analysis->get_app_last_analysis();
    $construct_array["analysis_code"] = $last_analysis["result"]["analysisCode"];

    $kiuwan_defects = new kiuwan_defects($construct_array);
    $defects_analysis = $kiuwan_defects->get_analysis_code_defects();
    //$files_analysis =   $kiuwan_defects->get_app_analysis_files();

    $compare = array("characteristic" => array(
                                        "Efficiency",
                                        "Maintainability",
                                        "Portability",
                                        "Reliability"
                    ),"priority" => array(
                                    "Very high",
                                    "High",
                                    "Normal",
                                    "Low",
                                    "Very Low"
                    )
                );

    //:( this is temporally
    function getCharPriorityCount($input,$priority,$characteristic){
        $count = 0;
        foreach ( $input as $value ) {
          if ( $value["priority"] == $priority && $value["characteristic"] == $characteristic ) {
            $count++;
          }
        }

        return $count;
    }


    function getCharCount($input,$characteristic){
        $count = 0;
        foreach ( $input as $value ) {
          if ($value["characteristic"] == $characteristic ) {
            $count++;
          }
        }

        return $count;
    }

    function getPriorityCount($input,$priority){
        $count = 0;
        foreach ( $input as $value ) {
          if ( $value["priority"] == $priority) {
            $count++;
          }
        }

        return $count;
    }

    $eficiencia = round($last_analysis["result"]["Quality indicator"]["children"][0]["value"],2);
    $mantenibilidad = round($last_analysis["result"]["Quality indicator"]["children"][1]["value"],2);
    $portabilidad = round($last_analysis["result"]["Quality indicator"]["children"][2]["value"],2);
    $confiabilidad = round($last_analysis["result"]["Quality indicator"]["children"][3]["value"],2);
    $global_indicator = round($last_analysis["result"]["Quality indicator"]["value"],2);

?>

<?php
    //var_dump($last_analysis["result"]);
    if($last_analysis["error"] === NULL){
        $html = "
        <html>
            <head>
                <link href=\"http://phptopdf.com/bootstrap.css\" rel=\"stylesheet\">
                <link href=\"http://getbootstrap.com/examples/dashboard/dashboard.css\" rel=\"stylesheet\">
            </head>
        <body>
            <div class=\"container\">
                <div class=\"row\">
                    <div class='main text-right'>
                        <h3>".date("d-m-Y", strtotime($last_analysis["result"]["date"]))."</h3>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class='row text-center'>
                    <div class=\"main\">
                        <img src='claro-logo.jpg'/>
                    </div>
                </div>
                <div class='row text-center'>
                    <div class=\"main\">
                        <h2>CLARO CHILE S.A</h2>
                        <h3>Informe de Resultados Líneas Bases-".$last_analysis["result"]["name"]."-Inspección de Código</h3>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class=\"row\">
                    <div class=\"main\">
                        <h3>CONTENIDO</h3>
                        <table class='table table-hover'>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Introduccion</td>
                                    <td>..........................</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>".$last_analysis["result"]["name"]."</td>
                                    <td>..........................</td>
                                    <td>2</td>
                                </tr> 
                                <tr>
                                    <td>2.1.1.</td>
                                    <td>Indicadores Principales</td>
                                    <td>..........................</td>
                                    <td>2</td>
                                </tr> 
                                <tr>
                                    <td>2.1.2.</td>
                                    <td>Defectos</td>
                                    <td>..........................</td>
                                    <td>3</td>
                                </tr>                                                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class=\"row\">
                    <h2>1. Introducción</h2>
                    <br>
                    <br>
                    <table class='table table-hover'>
                    <tbody>
                        <tr>
                            <td><p>El presente documento tiene como finalidad exponer los resultados obtenidos a partir de la línea base generada por el <strong>".$last_analysis["result"]["name"]."</strong>, analizado a través de la herramienta <strong>Kiuwan de Optimyth</strong></p></td>
                        </tr>
                        <tr>
                            <td><p>Para efectos de dichos análisis se ha considerado el modelo de calidad que trae por defecto Kiuwan, denominado <strong>".$last_analysis["result"]["quality_model"]."</strong>, cuya versión utilizada para construir las líneas bases es <strong>v3</strong>. </p></td>
                        </tr> 
                        <tr>
                            <td><p>Los resultados que podrán ser consultados son Información general del aplicativo, Indicador por característica y defectos por característica y prioridad.</p></td>
                        </tr>                                                               
                    </tbody>
                </table>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class=\"row\">
                    <h2>2. Aplicación y Análisis de la Información</h2>
                    <div class=\"main\">";
        //REMOVED CODE HERE
              
        $html.="
        <h3>2.1 ".$last_analysis["result"]["name"]."</h3>
        <table class='table table-bordered table-hover'>
            <tbody>
                <tr>
                    <td>Etiqueta</td>
                    <td>".$last_analysis["result"]["label"]."</td>
                </tr>
                <tr>
                    <td>Fecha Analisis</td>
                    <td>".$last_analysis["result"]["date"]."</td>
                </tr>
                <tr>
                    <td>Lenguajes Encontrados</td>
                    <td>".implode(', ', array_map(function($el){ return $el['name']; }, $last_analysis["result"]["languages"]))."</td>
                </tr>
                <tr>
                    <td>Archivos Analizados</td>
                    <td>".$last_analysis["result"]["Main metrics"][3]["value"]."</td>
                </tr>
                <tr>
                    <td>Líneas de Código (LDC)</td>
                    <td>".$last_analysis["result"]["Main metrics"][4]["value"]."</td>
                </tr>
                <tr>
                    <td>Valor del negocio</td>
                    <td>".$last_analysis["result"]["applicationBusinessValue"]."</td>
                </tr>
                <tr>
                    <td>Nombre del Modelo</td>
                    <td>".$last_analysis["result"]["quality_model"]."</td>
                </tr>
                <tr>
                    <td>Versión del Modelo</td>
                    <td>v3.0</td>
                </tr>                                                                    
            </tbody>
        </table>
        ";   
        $html.="
        <h3>2.1.1. Indicadores Principales</h3>
        <h4>INDICADOR GLOBAL: ".$global_indicator."</h4>
        <table class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th>CARACTERISTICA</th>
                    <th>INDICADOR OBTENIDO</th>
                    <th>OBJETIVO</th>
                    <th>OBSERVACIONES</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Eficiencia</td>
                    <td>".$eficiencia."</td>
                    <td>70</td>
                    ".(($eficiencia >= 70) ? '<td class="success"><p class="text-success">OK</p></td>' : '<td class="danger"><p class="text-danger">NOK</p></td>')."
                </tr>
                <tr>
                    <td>Mantenibilidad</td>
                    <td>".$mantenibilidad."</td>
                    <td>70</td>
                    ".(($mantenibilidad >= 70) ? '<td class="success"><p class="text-success">OK</p></td>' : '<td class="danger"><p class="text-danger">NOK</p></td>')."
                </tr>
                <tr>
                    <td>Portabilidad</td>
                    <td>".$portabilidad."</td>
                    <td>70</td>
                    ".(($portabilidad >= 70) ? '<td class="success"><p class="text-success">OK</p></td>' : '<td class="danger"><p class="text-danger">NOK</p></td>')."
                </tr>
                <tr>
                    <td>Confiabilidad</td>
                    <td>".$confiabilidad."</td>
                    <td>70</td>
                    ".(($confiabilidad >= 70) ? '<td class="success"><p class="text-success">OK</p></td>' : '<td class="danger"><p class="text-danger">NOK</p></td>')."
                </tr>                        
            </tbody>
        </table>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        ";
        
        
        $html.="
            <h3>2.1.2. Defectos</h3>
            <table class='table table-bordered table-hover'>
                <thead>
                    <tr>
                        <th> </th>
                        <th>VERY HIGH</th>
                        <th>HIGH</th>
                        <th>NORMAL</th>
                        <th>LOW</th>
                        <th>VERY LOW</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Eficiencia</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][0],$compare["characteristic"][0])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][1],$compare["characteristic"][0])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][2],$compare["characteristic"][0])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][3],$compare["characteristic"][0])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][4],$compare["characteristic"][0])."</td>
                    <td>".getCharCount($defects_analysis["result"]["defects"],$compare["characteristic"][0])."</td>
                </tr>
                <tr>
                    <td>Mantenibilidad</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][0],$compare["characteristic"][1])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][1],$compare["characteristic"][1])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][2],$compare["characteristic"][1])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][3],$compare["characteristic"][1])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][4],$compare["characteristic"][1])."</td>
                    <td>".getCharCount($defects_analysis["result"]["defects"],$compare["characteristic"][1])."</td>
                </tr>
                <tr>
                    <td>Portabilidad</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][0],$compare["characteristic"][2])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][1],$compare["characteristic"][2])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][2],$compare["characteristic"][2])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][3],$compare["characteristic"][2])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][4],$compare["characteristic"][2])."</td>
                    <td>".getCharCount($defects_analysis["result"]["defects"],$compare["characteristic"][2])."</td>
                </tr>
                <tr>
                    <td>Confiabilidad</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][0],$compare["characteristic"][3])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][1],$compare["characteristic"][3])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][2],$compare["characteristic"][3])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][3],$compare["characteristic"][3])."</td>
                    <td>".getCharPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][4],$compare["characteristic"][3])."</td>
                    <td>".getCharCount($defects_analysis["result"]["defects"],$compare["characteristic"][3])."</td>
                </tr>
                <tr>
                    <td>Total</td>
                        <td>".getPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][0])."</td>            
                        <td>".getPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][1])."</td>            
                        <td>".getPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][2])."</td>            
                        <td>".getPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][3])."</td>            
                        <td>".getPriorityCount($defects_analysis["result"]["defects"],$compare["priority"][4])."</td>            
                    <td>".$defects_analysis["result"]["defects_count"]."</td>
                </tr>                         
                </tbody>
            </table>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class=\"row\">
                <div class=\"main\">
                    <table class='table table-hover'>
                        <tbody>
                            <tr>
                                <td><p>Si desea más información respecto al análisis realizado favor dirigirse al siguiente <a href='".$last_analysis["result"]["analysisURL"]."'><strong>ENLACE</strong></a> y consultar el apartado de “code analysis”. Y podrá revisar el resumen del análisis, y el detalle de los defectos generados durante el análisis.</p></td>
                            </tr>                                               
                        </tbody>
                    </table>
                </div>
            </div>
            ";                    
        
        $html.="
        <h2>Información General</h2>
        <table class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th>Versión</th>
                    <th>Fecha</th>
                    <th>Autor</th>
                    <th>Descripción del Cambio</th>
                    <th>Revisado por</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>2.0</td>
                <td>".date("d/m/Y")."</td>
                <td>Area SCM</td>
                <td>Creación del documento</td>
                <td>Area SCM</td>
            </tr>                        
            </tbody>
        </table>
        
        <table class='table table-bordered table-hover'>
            <tbody>
                <tr>
                    <td>Archivo:</td>
                    <td>".$last_analysis["result"]["name"]."_".date("d-m-Y")."</td>
                </tr>
                <tr>
                    <td>Título:</td>
                    <td>Informe Resultados Líneas Bases – ".$last_analysis["result"]["name"]." – Inspección de Código</td>
                </tr>
                <tr>
                    <td>Proyecto:</td>
                    <td>Servicio Continuo de Inspección de Código</td>
                </tr>
                <tr>
                    <td>Versión:</td>
                    <td>2.0</td>
                </tr>
                <tr>
                    <td>Cliente:</td>
                    <td>Claro Chile S.A</td>
                </tr> 
                <tr>
                    <td>Autores:</td>
                    <td>Area SCM</td>
                </tr>                                                        
            </tbody>
        </table>
        "; 
        
        $html.="
                </div>
            </div>";
         
        
        $html .="
        </body>
              ";
      
              ob_get_clean();
              //echo($html);
              
      
              $dompdf = new Dompdf();
              $dompdf->loadHtml($html);
      
              // (Optional) Setup the paper size and orientation
              $dompdf->setPaper('A4', 'portrait');
      
              // Render the HTML as PDF
              $dompdf->render();
      
              if (!file_exists("pdf/".$last_analysis["result"]["name"]."/".date("Y")."/".date("m"))) {
                  mkdir("pdf/".$last_analysis["result"]["name"]."/".date("Y")."/".date("m"), 0755, TRUE);
              }
              // Output the generated PDF to Browser
              $dompdf->stream($last_analysis["result"]["name"]."_".$last_analysis["result"]["label"]."_".date("d-m-Y").'pdf'); 
      
              //$pdf_gen = $dompdf->output($last_analysis["result"]["name"]."_".$last_analysis["result"]["label"]."_".date("d-m-Y").'pdf');
      
              //$full_path = "pdf";
              //file_put_contents($full_path, $pdf_gen);
    }
?>