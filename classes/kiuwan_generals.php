<?php
    class kiuwan_generals
    {
        private $user;
        private $pass;
        private $get_info_url;
        private $get_app_list_url;
        private $get_app_by_name_url;
        private $get_app_by_analisis_url;
        private $get_applications_last_analysis_url;
        private $get_analysis_by_name_url;
        private $get_analysis_defects_by_name;
        private $get_analysis_defects_by_code_url;
        
        public function kiuwan_generals(){
            $this->user = "brayyan.yanes";
            $this->pass = "u!2p3E*2!<Up";
            $this->get_info_url = "https://api.kiuwan.com/info";
        }

        public function get_user_info(){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->get_info_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->pass);
        
            $result = curl_exec($ch);
            $error = NULL;
            if (curl_errno($ch)) {
                $error =  'Error:' . curl_error($ch);
            }
            curl_close ($ch);
        
            return (object) array("result" => json_decode($result), "error" => $error);
        }

    }
?>