<?php
    class kiuwan_apps
    {
        private $user;
        private $pass;
        private $get_app_list_url;
        private $get_app_by_name_url;

        public function kiuwan_apps(){
            $this->user = "brayyan.yanes";
            $this->pass = "u!2p3E*2!<Up";
            $this->get_app_list_url = "https://api.kiuwan.com/apps/list";
        }

        public function get_app_list(){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->get_app_list_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->pass);
        
            $result = curl_exec($ch);
            $error = NULL;
            if (curl_errno($ch)) {
                $error =  'Error:' . curl_error($ch);
            }
            curl_close ($ch);

            return (object) array("result" => json_decode($result), "error" => $error);
        }

    }
?>