<?php
    class kiuwan_analysis
    {
        private $user;
        private $pass;
        private $get_applications_last_analysis_url;
        private $get_analysis_by_name_url;
        private $get_analysis_code_url;
        private $get_analysis_defects_by_name;
        private $get_analysis_defects_by_code_url;
        private $get_analysis_list_by_name_url;
        
        public function kiuwan_analysis($array){
            $this->user = "brayyan.yanes";
            $this->pass = "u!2p3E*2!<Up";
            $this->get_applications_last_analysis_url = "https://api.kiuwan.com/applications/last_analysis?application=".$array["name"];
            $this->get_analysis_list_by_name_url = "https://api.kiuwan.com/apps/".$array["name"]."/analyses";
            $this->get_analysis_code_url = "https://api.kiuwan.com/apps/analysis/".$array["analysis_code"];
        }

        public function get_app_last_analysis(){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->get_applications_last_analysis_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->pass);
        
            $result = curl_exec($ch);
            $error = NULL;
            if (curl_errno($ch)) {
                $error =  'Error:' . curl_error($ch);
            }
            curl_close ($ch);
        
            return array("result" => json_decode($result,true), "error" => $error);
        
        }
        
        public function get_app_analysis_list(){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->get_analysis_list_by_name_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->pass);
        
            $result = curl_exec($ch);
            $error = NULL;
            if (curl_errno($ch)) {
                $error =  'Error:' . curl_error($ch);
            }
            curl_close ($ch);
        
            return array("result" => json_decode($result,true), "error" => $error);
        
        }

        public function get_app_analysis_by_id(){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->get_analysis_code_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->pass);
        
            $result = curl_exec($ch);
            $error = NULL;
            if (curl_errno($ch)) {
                $error =  'Error:' . curl_error($ch);
            }
            curl_close ($ch);
        
            return array("result" => json_decode($result,true), "error" => $error);
        
        }
    }
?>