<?php
    class kiuwan_defects
    {
        private $user;
        private $pass;
        private $get_analysis_defects_by_name;
        private $get_analysis_defects_by_code_url;
        private $get_analysis_files_by_code_url;

        public function kiuwan_defects($array){
            $this->user = "brayyan.yanes";
            $this->pass = "u!2p3E*2!<Up";
            $this->get_analysis_defects_by_code_url = "https://api.kiuwan.com/apps/analysis/".$array["analysis_code"]."/defects";
            $this->get_analysis_files_by_code_url = "https://api.kiuwan.com/apps/analysis/".$array["analysis_code"]."/files";
        }

        public function get_analysis_code_defects(){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->get_analysis_defects_by_code_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->pass);
        
            $result = curl_exec($ch);
            $error = NULL;
            if (curl_errno($ch)) {
                $error =  'Error:' . curl_error($ch);
            }
            curl_close ($ch);
        
            return array("result" => json_decode($result,true), "error" => $error);
        
        }
        
        public function get_app_analysis_files(){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->get_analysis_files_by_code_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->pass);
        
            $result = curl_exec($ch);
            $error = NULL;
            if (curl_errno($ch)) {
                $error =  'Error:' . curl_error($ch);
            }
            curl_close ($ch);
        
            return array("result" => json_decode($result,true), "error" => $error);
        
        }

    }
?>