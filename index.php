<?php
    include_once("classes/kiuwan_generals.php");
    include_once("classes/kiuwan_apps.php");
    $kiuwan_generals = new kiuwan_generals();
    $kiuwan_apps = new kiuwan_apps();
    $user_info = $kiuwan_generals->get_user_info();
?>
<html>
    <head>
        <link href="http://phptopdf.com/bootstrap.css" rel="stylesheet">
        <link href="http://getbootstrap.com/examples/dashboard/dashboard.css" rel="stylesheet">
        <link rel="stylesheet" href="AdminLTE.min.css">  
        <link rel="stylesheet" href="mcss.css"> 
        <link rel="stylesheet" href="_all-skins.min.css"> 
    </head>
    <body>
        <div class="container">
            <div class="row">
    
            </div>
            <div class="row">
                <div class="box-body">
                    <table class="table table-bordered table-hover" id="datatable">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Valor</th>
                            <th>Ultimo Analisis</th>
                            <th>Ver Lista Analisis</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $app_list = $kiuwan_apps->get_app_list()->result;
                                foreach ($app_list as $key) {
                            ?>
                        <tr>
                            <td><?php echo($key->name) ?></td>
                            <td><?php echo($key->description) ?></td>
                            <td><?php echo($key->applicationBusinessValue) ?></td>
                            <td><a href="ultimo_analisis.php?application=<?php echo($key->name); ?>" target="_blank">Ultimo Analisis</a></td>
                            <td><a href="lista_analisis.php?application=<?php echo($key->name); ?>">Lista analisis</a></td>
                        </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(function(e) {
            $('#datatable').DataTable(); 
        });
    </script>
</html>
