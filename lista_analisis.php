<?php
    include_once("classes/kiuwan_generals.php");
    include_once("classes/kiuwan_apps.php");
    include_once("classes/kiuwan_analisis.php");
    include_once("classes/kiuwan_defects.php");
    $application_name = $_GET["application"];

    $construct_array= array();
    $construct_array["name"] = $application_name;

    $kiuwan_generals = new kiuwan_generals();
    $kiuwan_apps = new kiuwan_apps();
    $kiuwan_analysis = new kiuwan_analysis($construct_array);

    $user_info = $kiuwan_generals->get_user_info();
?>
<html>
    <head>
        <link href="http://phptopdf.com/bootstrap.css" rel="stylesheet">
        <link href="http://getbootstrap.com/examples/dashboard/dashboard.css" rel="stylesheet">  
        <link rel="stylesheet" href="AdminLTE.min.css">
        <link rel="stylesheet" href="mcss.css">
        <link rel="stylesheet" href="_all-skins.min.css">  
    </head>
    <body>
        <div class="container">
            <div class="row">
    
            </div>
            <div class="row">
                <div class="box-body">
                    <table class="table table-bordered table-hover" id="datatable">
                        <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Etiqueta</th>
                            <th>Fecha Creacion</th>
                            <th>Modelo CQM</th>
                            <th>Invocador</th>
                            <th>Status</th>
                            <th>Scope</th> 
                            <th>Ver Reporte</th>                             
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $analysis_list = $kiuwan_analysis->get_app_analysis_list();
                            foreach ($analysis_list["result"] as $key) {
                        ?>
                        <tr>
                            <td><?php echo($key["code"]) ?></td>
                            <td><?php echo($key["label"]) ?></td>
                            <td><?php echo($key["creationDate"]) ?></td>
                            <td><?php echo($key["qualityModel"]) ?></td>
                            <td><?php echo($key["invoker"]) ?></td>
                            <td><?php echo($key["status"]) ?></td>
                            <td><?php echo($key["analysisScope"]) ?></td>
                            <td><a href="analisis_especifico.php?analysis_code=<?php echo($key["code"]); ?>" target="_blank">Ver Reporte</a></td>
                        </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(function(e) {
            $('#datatable').DataTable(); 
        });
    </script>
</html>
